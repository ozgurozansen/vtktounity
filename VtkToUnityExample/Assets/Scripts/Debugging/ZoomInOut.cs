﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomInOut : MonoBehaviour
{
    float camZoom = -0.4f;
    float camZoomSpeed = 0.15f;
    Transform Cam;

    // Start is called before the first frame update
    void Start()
    {
        Cam = this.transform;
    }

    void Update()
    {
        camZoom += Input.GetAxis("Mouse ScrollWheel") * camZoomSpeed;
        transform.position = new Vector3(Cam.position.x, Cam.position.y, camZoom);   // use localPosition if parented to another GameObject.
    }
}
